//
//  KRAFTWERKINGViewController.h
//  Prep
//
//  Created by RJ Militante on 1/16/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRAFTWERKINGAwesomeClass.h"

@interface KRAFTWERKINGViewController : UIViewController

@property (nonatomic) CGPoint currentPoint;

@property (strong, nonatomic) IBOutlet UIButton *myButton;

@property (strong, nonatomic) KRAFTWERKINGAwesomeClass *awesomeClass;

@end

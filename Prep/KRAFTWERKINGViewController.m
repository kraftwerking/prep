//
//  KRAFTWERKINGViewController.m
//  Prep
//
//  Created by RJ Militante on 1/16/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGViewController.h"

@interface KRAFTWERKINGViewController ()

@end

@implementation KRAFTWERKINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSString *firstString = @"First string";
    NSString *secondString = @"Second string";
    
    NSArray *myArray = [[NSArray alloc] initWithObjects:firstString, secondString, nil];
                        NSLog(@"%@", myArray);
    NSMutableArray *myMutableArray = [[NSMutableArray alloc] init];
    [myMutableArray addObject:firstString];
    [myMutableArray addObject:myArray];
    [myMutableArray addObject:secondString];
    NSLog(@"%@", myMutableArray);
    
    self.currentPoint= CGPointMake(3, 4);
    NSLog(@"%f %f", self.currentPoint.x, self.currentPoint.y);
    
    int x = 10;
    int y = 20;
    if(x == 10){
        if(y == 20){
            NSLog(@"Both are true");
        }
    }
    
    [self.myButton setTitle:@"Button pressed" forState:UIControlStateNormal];
    self.myButton.hidden = NO;
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"You triggered the alert!" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
    [alertView show];

    self.awesomeClass = [[KRAFTWERKINGAwesomeClass alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
